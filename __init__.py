import time
dna = 'ATGCGGACCTAT'
def count_v1(dna, base):
    dna = list(dna) # convert string to list of letters
    i=0
    for c in dna :
        if c == base :
            i += 1
    return i

def count_v2(dna, base) :
    i = 0
    for c in dna :
        if c == base :
            i += 1
    return i

def count_v3(dna, base):
    i = 0 # counter
    for j in range(len(dna)) :
        if dna[j] == base :
            i += 1
    return i

def count_v4(dna, base) :
    i = 0
    j = 0 # string index
    while j < len(dna) :
        i += 1
    j += 1
    return i

def count_v5(dna, base) :
    m = [] # matches for base in dna: m[i]=True if dna[i]==base
    for c in dna :
        if c == base :
            m.append(True)
        else :
            m.append(False)
    return sum(m)

def count_v6(dna, base) :
    m = []
    for c in dna :
        m.append(True if c == base else False)
    return sum(m)

# Using boolean values directly
def count_v7(dna, base) :
    m = []
    for c in dna :
        m.append(c==base)
    return sum(m)

# List comprehensions
def count_v8(dna, base) :
    m = [c == base for c in dna]
    return sum(m)

def count_v9(dna, base) :
    return sum([c==base for c in dna])

def count_v10(dna, base) :
    return sum(c==base for c in dna)

def count_v11(dna, base) :
    return len([i for i in range(len(dna)) if dna[i] == base])

#Using Python's Library!!!!
def count_v12(dna, base) :
    return dna.count(base)

#CODINGBAT
def missing_char(str, n) :
  str2 = ""
  for i in range(len(str)) :
    if(i != n) :
      str2 += str[i]
  return str2

def string_splosion(str):
  ret = ""
  count = 1
  for i in range(len(str)):
    ret+=str[0:i+count]
    count = count+1
  return ret

functions = [count_v1, count_v2, count_v3, count_v4, count_v5, count_v6, count_v7, count_v8, count_v9, count_v10, count_v11, count_v12]
timings = [] # timings[i] holds CPU time for functions[i]
for function in functions:
    t0 = time.clock()
    function(dna, 'A')
    t1 = time.clock()
    cpu_time = t1-t0
    timings.append(cpu_time)

for cpu_time, function in zip(timings, functions):
    print '{f:<9s}: {cpu:2f} s'.format(f=function.func_name, cpu = cpu_time)


