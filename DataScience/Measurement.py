import numpy as np
import datetime as dt
import matplotlib.pyplot as plt

file_name = "measurement_20180119.npy"
data = np.load(file_name)
print data

# Convert the timestamp to
time = [dt.datetime.fromtimestamp(ele[0]/1000).strftime('%Y-%m-%d %H:%M:%S') for ele in data]

#plotting the data
fig, ax = plt.subplots()
#ax.plot(time, data[:,1])
ax.plot(data[:,1])
plt.show()