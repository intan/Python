from sympy import exp, sin, cos, symbols, simplify, expand
t = symbols('t')
f = exp(t)
#print f.series(t, 0, 3) #Taylorpolynom

f = exp(sin(t))
#print f.series(t, 0, 8)

x, y = symbols('x y')
f = -sin(x)*sin(y) + cos(x)*cos(y)
#print simplify(f)

#print expand(sin(x+y), trig=True)

real = 6; imag = 3.1
#print complex(real, imag)

x=1; print 'sin(%g)=%g' % (x, sin(x))