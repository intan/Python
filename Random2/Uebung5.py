import pandas as pd
from datetime import datetime

daten = pd.read_csv('Intanv2.csv') #Die Quelldatei wurde ausgelesen

def diff(zeit):
    """Funktion, die den gewuenschten Zeitabstand zurueckgibt"""
    return int(zeit)

diff = diff(60) #60 Sekunden

def tStart(start): #start = daten['Unix;Timestamp'][0]
    """Funktion, die die erste Unixtimestamp in der Ausgabedatei zurueckgibt"""
    if start % 60 > 30:
        ret = 60 - (start % 60)
        ret = ret + start
    else:
        ret = start - (start % 60)
    return ret

def tEnde(ende): #ende = daten['Unix;Timestamp'][len(daten)-1]
    """Funktion, die die letzte Unixtimestamp in der Ausgabedatei zurueckgibt"""
    if ende % 60 > 30:
        ret = 60 - (ende % 60)
        ret = ret + ende
    else:
        ret = ende - (ende % 60)
    return ret

def steigung(V1,V2,T1,T2):
    """Funktion, die eine Steigung berechnet."""
    Av = V2 - V1
    At = T2 - T1
    if T1 == T2:
        return 0
    else:
        return Av/float(At)

Tstart = tStart(daten['Unix;Timestamp'][0]) #Die erste Unix Timestamp in der Datenmenge

Tende = tEnde(daten['Unix;Timestamp'][len(daten)-1]) #Die letzte Unix Timestamp in der Datenmenge

f = open('csv.csv', mode='w') #csv ist die Ausgabedatei
f.write("Time,Value\n") #Time und Value sind die Attribute von csv

i=0
while i < len(daten)-1:
    T1 = daten['Unix;Timestamp'][i] #Die Unix Timestamp des Index i
    V1 = daten['Value'][i] #Die elektrische Leistung des Index i
    D1 = daten['Time'][i][14:16] #Die Minute des Index i
    T2 = daten['Unix;Timestamp'][i+1] #Die Unix Timestamp des Index i+1
    V2 = daten['Value'][i+1] #Die elektrische Leistung des Index i+1
    D2 = daten['Time'][i+1][14:16] #Die Minute des Index i+1
    m = steigung(V1,V2,T1,T2)

    if D1 == D2: #mehrere Werte innerhalb einer Minute
        sumVal = 0
        counter = 1
        while (i<len(daten)-1 and D1==D2):
            i+=1
            sumVal += V1
            counter += 1
            D1 = daten['Time'][i][14:16]  # Die Minute des Index i
            D2 = daten['Time'][i+1][14:16]  # Die Minute des Index i+1
            V1 = daten['Value'][i]  # Die elektrische Leistung des Index i
        sumVal += V1
        average = float(sumVal)/counter
        f.write((datetime.utcfromtimestamp(Tstart)).strftime('%d.%m.%Y %H:%M:%S') + ",")  # Unix Timestamp->Normales Datum
        f.write("%s" % str(average) + "\n")
        Vrecent = sumVal
        i+=1
        Tstart += diff
        """
        if i == 7:
            m = steigung(Vrecent, V1, Tstart, T1)
            Vende = (m * (T1 - Tstart)) + Vrecent
            f.write((datetime.utcfromtimestamp(Tende)).strftime(
                '%d.%m.%Y %H:%M:%S') + ",")  # Unix Timestamp->Normales Datum
            f.write("%s" % str(Vende) + "\n")
            break
        """

    else:
        while Tstart < T2-diff:
            preVal = (m * (Tstart-T1)) + V1
            f.write((datetime.utcfromtimestamp(Tstart)).strftime('%d.%m.%Y %H:%M:%S') + ",") #Unix Timestamp->Normales Datum
            f.write("%s" % str(preVal) + "\n")
            Tstart += diff
        i+=1
        """
        if i == 7:
            m = steigung(V1, V2, T1, T2)
            Vende = (m * (T2 - Tende)) + V1
            f.write((datetime.utcfromtimestamp(Tende)).strftime('%d.%m.%Y %H:%M:%S') + ",")  # Unix Timestamp->Normales Datum
            f.write("%s" % str(Vende) + "\n")
            break
        """






