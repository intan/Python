# coding=utf-8

import pandas as pd
import datetime
import time

zeit = [] #Speicherplatz für Unix Timestamp von csv Daten

daten = pd.read_csv('csv')

def steigung(v1, v2, t1, t2): #Funktion, die Steigung berechnet
    Av = v2 - v1
    At = t2 - t1
    if t1 == t2 :
        return 0
    else :
        return Av/float(At)

for i in range(len(daten)):
    a = pd.to_datetime(daten['Time'][i]) #string -> datetime
    zeit.append(float(time.mktime(a.timetuple()))) # datetime -> Unix Timestamp

f = open('Intancsv.csv', mode='w')
f.write("Time,Value\n")
for i in range(len(daten)-1):
    v1 = daten['Value'][i]
    v2 = daten['Value'][i+1]
    t1 = zeit[i]
    t2 = zeit[i+1]
    preVal = 0 #Value, die vorhersagt wird
    time = t1+60 #Zeit, die nicht in der Datei drin ist
    m = steigung(v1, v2, t1, t2)
    # Unix Timestamp -> datetime
    f.write("%s,%s" % (datetime.datetime.fromtimestamp(t1).strftime('%Y-%m-%d %H:%M:%S'),v1) + "\n")
    while time < t2 :
        preVal = (m * (time-t1)) + v1
        f.write("%s,%s" % (datetime.datetime.fromtimestamp(time).strftime('%Y-%m-%d %H:%M:%S'), str(preVal)) + "\n")
        time = time + 60

f.write("%s,%s" % (datetime.datetime.fromtimestamp(zeit[len(daten)-1]).strftime('%Y-%m-%d %H:%M:%S'), daten['Value'][len(daten)-1]))


