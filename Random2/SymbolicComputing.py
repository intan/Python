from sympy import (
symbols,
diff,
integrate,
Rational,
lambdify,
solve
)

t, v0, g = symbols('t v0 g')
y = v0*t - Rational(1,2)*g*t**2
dydt = diff(y,t)
#print dydt
#print 'acceleration:', diff(y, t, t) #2nd derivative
#print integrate(dydt, t)

v = lambdify([t, v0, g], dydt)
print v(0, 5, 9.81)
roots = solve(y,t)
print roots
print y.subs(t, roots[1])