from matplotlib.pylab import *

""""
def f(t):
    return t**2*exp(-t**2)

t = linspace(0, 3, 31)
y = zeros(len(t))
for i in xrange(len(t)):
    y[i] = f(t[i])

plot(t,y)
xlabel('t')
ylabel('y')
legend(['t^2*exp(-t^2)'])
axis([0, 3, -0.05, 0.6]) #[tmin, tmax, ymin, ymax]
title("My First Matplotlib Demo")
savefig("tmp2.pdf")
show()
"""

def f1(t):
    return t**2*exp(-t**2)

def f2(t):
    return t**2*f1(t)

t = linspace(0, 3, 51)
y1 = f1(t)
y2 = f2(t)

plot(t, y1, 'r-')
hold('on')
plot(t, y2, 'bo')
xlabel('t')
ylabel('t')
legend(['t^2*exp(-t^2)', 't^4*exp(-t^2)'])
title("Plotting two curves in the same plot")

figure()
subplot(2,1,1) #subplot(r, c, a) r:rows, c:columns, a:row-wise
t = linspace(0,3,51)
y1 = f1(t)
y2 = f2(t)

plot(t, y1, 'r-', t, y2, 'bo')
xlabel('t')
ylabel('y')
axis(t[0], t[-1], min(y2)-0.05, max(y2)+0.5)
legend(['t^2*exp(-t^2)', 't^4*exp(-t^2)'])
title("Top Figure")

subplot(2,1,2)
t3 = t[::4]
y3=f2(t3)

plot(t,y1, 'b-', t3, y3, 'ys')
xlabel('t')
ylabel('y')
axis([0,4, -0.2, 0.6])
legend(['t^2*exp(-t^2)', 't^4*exp(-t^2)'])
savefig('tmp4.pdf')
show()