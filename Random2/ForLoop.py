import pprint
n = 21
C_min = -10
C_max = 40
dC = (C_max - C_min)/float(n-1) # increment
Cdegrees = []
for i in range(0, n):
    C = -10 + i*dC
    Cdegrees.append(C)

Fdegrees = []
for C in Cdegrees:
    F = (9.0/5)*C + 32
    Fdegrees.append(F)

"""for i in range(len(Cdegrees)):
    print '%5.1f %5.1f' % (Cdegrees[i],Fdegrees[i])"""

"""for c in Cdegrees:
    c += 5
print Cdegrees

   for i in range(len(Cdegrees)):
    Cdegrees[i] += 5
print Cdegrees"""

"""C_plus_5 = [C+5 for C in Cdegrees]
print C_plus_5"""

Cdegrees2 = range(-20, 40, 5)
Fdegrees2 = [(9.0/5)*C + 32 for C in Cdegrees]
table = [Cdegrees2, Fdegrees2]
print table

table = []
for C, F in zip(Cdegrees2, Fdegrees2):
    table.append([C,F])
print table

for C, F in zip(Cdegrees2, Fdegrees2):
    print '%5.1f %5.1f' % (C,F)

pprint.pprint(table)