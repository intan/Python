import csv
from datetime import datetime
import time
import calendar

def steigung(V1,V2,T1,T2):
    """Funktion, die eine Steigung berechnet."""
    Av = V2 - V1
    At = T2 - T1
    if T1 == T2:
        return 0
    else:
        return Av/float(At)

Tstart = calendar.timegm(time.strptime('08.07.2017 08:20:00', '%d.%m.%Y %H:%M:%S'))
#Die erste Unix Timestamp in der Datenmenge

Tende = calendar.timegm(time.strptime('26.07.2017 08:13:00', '%d.%m.%Y %H:%M:%S'))
#Die letzte Unix Timestamp in der Datenmenge

#with open ('Intanv2.csv','rb') as f:
    #csv_reader = csv.DictReader(f)


csv_reader = csv.DictReader(open("Intanv2.csv"))

oldValue = 0
with open('csv.csv', 'wb') as new_file:
    fieldnames = ['Time', 'Value']
    csv_writer = csv.DictWriter(new_file, fieldnames=fieldnames, delimiter= ',')
    csv_writer.writeheader()

for line in csv_reader:
    diff = int(line['Value']) - oldValue #
    oldValue = int(line['Value'])
    print diff





