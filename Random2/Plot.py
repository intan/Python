import numpy as np
import matplotlib.pyplot as plt

X = np.linspace(-np.pi, np.pi, 256,endpoint=True)
"""X is now a numpy array with 256 values ranging from -π to +π (included). 
 C is the cosine (256 values) and S is the sine (256 values)."""
C,S = np.cos(X), np.sin(X)

plt.plot(X,C)
plt.plot(X,S)

plt.show()