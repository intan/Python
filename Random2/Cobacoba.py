import pandas as pd

data = pd.read_csv("Linear.csv")
data = pd.DataFrame(data)

X = data[['x']] #type = DataFrame
y = data.y #type = Series

"""from sklearn.cross_validation import train_test_split
X_train, x_test, y_train, y_test = train_test_split(X,y, random_state=1)"""

from sklearn.linear_model import LinearRegression
linreg = LinearRegression()
print linreg.fit(X,y)

print linreg.intercept_
print linreg.coef_

X_predict = [[3]]
y_predict = linreg.predict(X_predict)
print y_predict


